=======
History
=======

* ENH - add option to print day's calories entries. #5

0.3.3 (2022-03-20)
------------------

* ENH - split plot into columns. #3
* ENH - print summary of recent inputs when entering. #4

0.3.0 (2022-03-08)
------------------

* Bugfix - Creates header when making new log csv file.
* Bugfix - Generic to any home directory


0.2.0 (2022-03-08)
------------------

* Added calorie tracking.


0.1.0 (2021-03-14)
------------------

* First release on PyPI.
