====
mvwt
====


.. image:: https://img.shields.io/pypi/v/mvwt.svg
        :target: https://pypi.python.org/pypi/mvwt

.. image:: https://img.shields.io/travis/Craskermasker/mvwt.svg
        :target: https://travis-ci.com/Craskermasker/mvwt

.. image:: https://readthedocs.org/projects/mvwt/badge/?version=latest
        :target: https://mvwt.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Minimum viable weight tracker


* Free software: GNU General Public License v3
* Documentation: https://mvwt.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
